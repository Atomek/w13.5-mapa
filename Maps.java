package maps;

import java.util.HashMap;
import java.util.Map;

public class Maps {
    public static void main(String[] args) {
        Map<Double, Double> numberMap = new HashMap<>();

        for (double counterOfKey = -10; counterOfKey <= 10; counterOfKey +=0.5) {
            numberMap.put(counterOfKey, Math.pow(counterOfKey, 2));
        }

        printMap(numberMap);
    }

    private static void printMap(Map<Double, Double> numberMap) {
        System.out.printf("%4s"+ "\t\t" + "%5s","key" , "value");
        System.out.println();
        for (double key = -10; key <= 10 ; key+=0.5) {
            System.out.printf("%4.1f" + "\t\t" + "%2s",key, numberMap.get(key));
            System.out.println();
        }
    }
}
